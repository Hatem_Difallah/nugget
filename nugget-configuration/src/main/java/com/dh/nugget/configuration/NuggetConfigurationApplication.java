package com.dh.nugget.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;


@SpringBootApplication
@EnableConfigServer
public class NuggetConfigurationApplication {
	public static void main(String[] args) {
		SpringApplication.run(NuggetConfigurationApplication.class, args);
	}
}
