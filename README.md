# nugget
Microservices with Spring Cloud Demo nugget
In this project I'm demonstrating you the most interesting features of Spring Cloud Project for building microservice-based architecture. Most of examples are based on Spring Boot 1.5.

# Getting Started
Currently you may find here some examples of microservices implementation using different projects from Spring Cloud. All the examples are divided into the branches and described in a separated articles on my blog. Here's a full list of available examples:

Introduction to Spring Cloud components like discovery with Eureka, load balancing with Ribbon, REST client Feign, API gataway with Zuul. The example is available in the branch master. A detailed description can be found here: 

 # Creating microservice using Spring Cloud, Eureka and Zuul
