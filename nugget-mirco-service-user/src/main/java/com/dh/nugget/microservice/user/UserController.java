package com.dh.nugget.microservice.user;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("user")
@RefreshScope
@CrossOrigin("*")
public class UserController {
	  
  @Autowired
  private UserRepository userRepository;
  
  @Value("${xParam}")
  private int xParam;
  
  /**
   * getById
   * 
   * @param id Long
   * @return User
   */
  @GetMapping("/{id}")
  public User getById(@PathVariable Long id){
    User user = userRepository.getById(id);
    return user;
  }
  
  /**
   * findAll
   * 
   * @return List<User>
   */
  @GetMapping("/users")
  public List<User> findAll(){
   List<User>  users = userRepository.findAll();
   return users;
  }
  /**
   * MyConfig
   * 
   * @return
   */
  @GetMapping("/MyConfig")
  public Map<String, Object> MyConfig(){
	  Map<String, Object> myConfig = new HashMap<>();
	  myConfig.put("xParam", xParam);
	  myConfig.put("threadName", Thread.currentThread().getName() );
	  return myConfig ;  
  }
}
