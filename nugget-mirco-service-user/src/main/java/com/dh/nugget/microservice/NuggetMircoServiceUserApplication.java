package com.dh.nugget.microservice;

import java.util.stream.Stream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

import com.dh.nugget.microservice.user.User;
import com.dh.nugget.microservice.user.UserRepository;

@EnableDiscoveryClient
@SpringBootApplication
public class NuggetMircoServiceUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(NuggetMircoServiceUserApplication.class, args);
	}

	@Bean
	CommandLineRunner start(UserRepository userRepository){
		
		User user1 = new User(null , "Difallah" , "hatem" ,"rotating_card_profile.png" ,"computer engineer" ,"fullstack JAVA/ANGULAR") ;
		User user2 = new User(null , "Dhifallah" , "Soufien" ,"rotating_card_profile2.png" ,"computer engineer" ,"JEE") ;
		User user3 = new User(null , "Elj" , "Slim" ,"rotating_card_profile3.png" ,"computer engineer" ,"ANGULARJS") ;
		return args ->{
			Stream.of(user1 , user2 , user3).forEach(user -> {
				userRepository.save(user);
			});
			userRepository.findAll().forEach(System.out::println);
		};
	}
}
