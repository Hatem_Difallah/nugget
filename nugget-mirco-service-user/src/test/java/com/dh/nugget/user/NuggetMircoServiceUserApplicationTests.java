package com.dh.nugget.user;

import javax.ws.rs.core.Application;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = Application.class)
class NuggetMircoServiceUserApplicationTests {

	@Test
	void contextLoads() {
	}

}
